package services

import (
	"context"
	"temp/genproto/customer"
	"temp/message_broker/models"
	"temp/pkg/logger"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)


func (c *CustomService) SendSMSVerificationCode(ctx context.Context, req *customer.SendSMSVerificationCodeReq) (*customer.Empty, error) {
	err := c.kafkaProducer.SendSMSVerification(&models.SendSMSVerificationCodeReq{
		To: req.To,
		Code: req.Code,
	})
	if err != nil {
		c.Logger.Error("Error while Sending Email", logger.Error(err))
		return &customer.Empty{}, status.Error(codes.Internal, "Couldn`t send email you")
	}

	return &customer.Empty{}, err

}