package services

import (
	"context"
	"fmt"
	pc "temp/genproto/customer"
	pp "temp/genproto/post"
	"temp/genproto/review"

	// "temp/genproto/post"

	"temp/pkg/logger"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (c *CustomService) CreateCustomer(ctx context.Context, req *pc.CustomerReq) (*pc.CustomerResp, error) {
	customers, err := c.Storage.Customer().CreateCustomer(req)
	if err != nil {
		c.Logger.Error("Error while creating customer", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, "Please check your data")
	}

	return customers, nil
}

func (c *CustomService) UpdateCustomer(ctx context.Context, req *pc.CustomerResp) (*pc.CustomerResp, error) {
	customers, err := c.Storage.Customer().UpdatedCustomer(req)
	if err != nil {
		c.Logger.Error("Error while updating customer", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, "Please check your data")
	}
	return customers, nil
}

func (c *CustomService) GetCustomers(ctx context.Context, req *pc.CustomerID) (*pc.CustomerResp, error) {
	customer, err := c.Storage.Customer().GetCustomers(req)
	if err != nil {
		c.Logger.Error("Error while getting customer", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, "Please check your data")
	}
	responce := &pc.CustomerResp{
		Id:          customer.Id,
		FirstName:   customer.FirstName,
		LastName:    customer.LastName,
		Wikipedia:   customer.Wikipedia,
		Address:     customer.Address,
		Email:       customer.Email,
		PhoneNumber: customer.PhoneNumber,
	}

	return responce, nil
}

func (c *CustomService) DeletedCustomer(ctx context.Context, req *pc.CustomerID) (*pc.Empty, error) {
	err := c.Storage.Customer().DeletedCustomer(req)
	if err != nil {
		c.Logger.Error("Error while deleting address and customer", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, "Please check your data")
	}
	return &pc.Empty{}, nil
}

// func (c *CustomService) GetAllCustomers(ctx context.Context, req *pc.CustomerID) (*pc.GetCustomer, error) {
// 	customer, err := c.Storage.Customer().GetCustomers(req)
// 	if err != nil{
// 		c.Logger.Error("Error with customer and address",logger.Any("Error with Service",err))
// 		return &pc.GetCustomer{}, status.Error(codes.Internal, "Please check your info")
// 	}
// }

func (c *CustomService) GetAllCustomers(ctx context.Context, req *pc.CustomerID) (*pc.GetCustomer, error) {
	fmt.Println(`GetAllCustomer Hello`)
	customer, err := c.Storage.Customer().GetCustomers(req)
	if err != nil {
		c.Logger.Error("Error with customer and addresses", logger.Any("Error with Service", err))
		return &pc.GetCustomer{}, status.Error(codes.Internal, "please check your info")
	}
	responce := &pc.GetCustomer{
		Id:        customer.Id,
		FirstName: customer.FirstName,
		LastName:  customer.LastName,
		Wikipedia: customer.Wikipedia,
		Address:   customer.Address,
	}
	fmt.Println(`GetReviewByPostID`)
	reviews, err := c.Client.Review().GetReviewByPostID(ctx, &review.ReviewID{ReviewID: responce.Id})
	fmt.Println(err,`This Error`)
	if err != nil {
		
		c.Logger.Error("Error with review", logger.Any("Error with Service", err))
		return nil, status.Error(codes.Internal, "please check your info")
	}
	for _, review := range reviews.Reviews {
		responce.Review = append(responce.Review, &pc.Review{
			Id:          review.Id,
			CustomerId:  review.CustomerId,
			PostId:      review.PostId,
			Rating:      review.Rating,
			Description: review.Description,
		})
	}
	fmt.Println(`GetPosts`)
	post, err := c.Client.Post().GetPosts(ctx, &pp.PostID{PostId: responce.Id})
	if err != nil {
		c.Logger.Error("Error with post and media", logger.Any("Error with Service", err))
		return nil, status.Error(codes.Internal, "please check your info")
	}

	post_ls := &pc.Post{
		Id:          post.Id,
		Name:        post.Name,
		Description: post.Description,
		About:       post.About,
	}

	for _, pos := range post.Media {
		post_ls.Media = append(post_ls.Media, &pc.Media{
			PostId: pos.PostId,
			Name:   pos.Name,
			Link:   pos.Link,
			Type:   pos.Type,
		})
	}
	responce.Post = post_ls

	return responce, err
}

func (c *CustomService) CheckField(ctx context.Context, req *pc.CheckFieldReq) (*pc.CheckFieldResp, error) {
	fmt.Println(`Enter`)
	field, err := c.Storage.Customer().CheckField(req)
	if err != nil {
		c.Logger.Error("Error with Checkfield, field and values", logger.Any("Error with Field", err))
		return &pc.CheckFieldResp{}, status.Error(codes.Internal, "please check your info")
	}
	fmt.Println(`Exit`)
	return field, nil
}

// func (c *CustomService) GetInfoCustomers(ctx context.Context, req *pc.CustomerID) (*pc.CustomerInfo, error) {
// 	customer, err := c.Storage.Customer().GetCustomers(req)
// 	if err != nil{
// 		c.Logger.Error("Error with customer and address",logger.Any("Error with Service",err))
// 		return nil, status.Error(codes.Internal,"please check your info")
// 	}
// 	posts, err := c.Client.Post().GetPostWithReview(ctx, &pp.PostID{PostId: customer.Id})
// 	if err != nil{
// 		c.Logger.Error("Error with post and media",logger.Any("Error with Service",err))
// 		posts = &pp.PostswithReview{}
// 	}
// 	for _, p := range posts.Posts{
// 		temp := &pc.Post{
// 			Id: p.Id,
// 			PostId: customer.Id,
// 			Name: p.Name,
// 			Description: p.Description,
// 			About: p.About,
// 		}
// 		for _, media := range p.Media{
// 			temp.Media = append(temp.Media, (*pc.Media)(media))
// 		}
// 		for _,review := range p.Review{
// 			temp.Review = append(temp.Review, (*pc.Review)(review))
// 		}
// 		customer.Posts = append(customer.Posts, temp)
// 	}
// return customer, nil
// }

func (c *CustomService) ListCustomers(ctx context.Context, req *pc.ListCustomersReq) (*pc.ListCustomersResp, error) {
	customer, err := c.Storage.Customer().ListCustomers(req.Limit, req.Page)
	if err != nil {
		c.Logger.Error("Error get list customers", logger.Any("Error with ListCustomers", err))
		return &pc.ListCustomersResp{}, status.Error(codes.Internal, "please check your info")
	}
	return customer, nil
}

func (c *CustomService) GetEmail(ctx context.Context, req *pc.LoginReq) (*pc.LoginResp, error) {
	email, err := c.Storage.Customer().GetByEmail(req.Email)
	if err != nil {
		c.Logger.Error("Error get by email", logger.Any("Error with Email", err))
		return &pc.LoginResp{}, status.Error(codes.Internal, "please check your info")
	}
	return email, nil
}

func (c *CustomService) GetCustomerBySearch(ctx context.Context, req *pc.GetListCustomerSearch) (*pc.ListCustomersResp, error) {
	customers, err := c.Storage.Customer().GetCustomerBySearch(req)
	if err != nil {
		c.Logger.Error("Error get by search Customer info", logger.Any("Error with Customer Search", err))
		return &pc.ListCustomersResp{}, status.Error(codes.Internal, "please check your info")
	}
	responce := &pc.ListCustomersResp{}
	for _, customer := range customers {
		temp := &pc.CustomerResp{
			Id:          customer.Id,
			FirstName:   customer.FirstName,
			LastName:    customer.LastName,
			Wikipedia:   customer.Wikipedia,
			Email:       customer.Email,
			PhoneNumber: customer.PhoneNumber,
			Password:    customer.Password,
			Code:        customer.Code,
		}
		responce.Customers = append(responce.Customers, temp)
	}
	return responce, nil
}

func (c *CustomService) GetAdmin(ctx context.Context, req *pc.GetAminReq) (*pc.GetAdminResp, error) {
	fmt.Println(req)
	admin, err := c.Storage.Customer().GetAdmin(req)
	fmt.Println(req)
	fmt.Println(admin)

	if err != nil {
		c.Logger.Error("Error get by name Admin Info", logger.Any(`Error with Admin`, err))
		return &pc.GetAdminResp{}, status.Error(codes.Internal, "please check your info")
	}
	return admin, nil
}

func (c *CustomService) GetModerator(ctx context.Context, req *pc.GetModeratorReq) (*pc.GetModeratorResp, error) {
	moderator, err := c.Storage.Customer().GetModerator(req)
	if err != nil {
		c.Logger.Error("Error get by name Moderator Info", logger.Any("Error with Moderator", err))
		return &pc.GetModeratorResp{}, status.Error(codes.Internal, "please check your info")
	}
	return moderator, nil
}
