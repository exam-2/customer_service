package client

import (
	"fmt"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"temp/config"
	pp "temp/genproto/post"
	pr "temp/genproto/review"
)

type Clients interface {
	Review() pr.ReviewServiceClient
	Post() pp.PostServiceClient
}
type ServiceManager struct {
	config        config.Config
	postService   pp.PostServiceClient
	reviewService pr.ReviewServiceClient
}

func New(c config.Config) (*ServiceManager, error) {
	post, err := grpc.Dial(
		fmt.Sprintf("%s:%s", c.PostServiceHost, c.PostServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))

	if err != nil {
		return &ServiceManager{}, err
	}
	review, err := grpc.Dial(
		fmt.Sprintf("%s:%s", c.ReviewServiceHost, c.ReviewServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return &ServiceManager{}, err
	}
	return &ServiceManager{
		config:        c,
		postService:   pp.NewPostServiceClient(post),
		reviewService: pr.NewReviewServiceClient(review),
	}, nil
}

func (p *ServiceManager) Post() pp.PostServiceClient {
	return p.postService
}
func (r *ServiceManager) Review() pr.ReviewServiceClient {
	return r.reviewService
}
