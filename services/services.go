package services

import (
	"temp/config"
	messagebroker "temp/message_broker"
	"temp/pkg/logger"
	"temp/services/client"
	"temp/storage"

	"github.com/jmoiron/sqlx"
)

type CustomService struct {
	Storage       storage.IStorage
	Logger        logger.Logger
	Client        client.Clients
	Cfg           *config.Config
	kafkaProducer *messagebroker.Producer
}

func NewCustomerService(db *sqlx.DB, log logger.Logger, client client.Clients, cfg *config.Config, kafka *messagebroker.Producer) *CustomService {
	return &CustomService{
		Storage: storage.NewStoragePg(db),
		Logger:  log,
		Client:  client,
		Cfg: cfg,
		kafkaProducer: kafka,
	}
}
