CREATE TABLE IF NOT EXISTS addresses (
    customer_id INT NOT NULL REFERENCES customers(id),
    district TEXT, 
    street TEXT,
    home_number TEXT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT now(),
    deleted_at TIMESTAMPTZ
)