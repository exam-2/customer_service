update_submodule:
	git submodule update --remote --merge

run:
	go run cmd/main.go

proto-gen:
	./script/gen-proto.sh

create_proto_submodule:
	git submodule add git@gitlab.com:exam-2/protos.git

migrate_up:
	migrate -path migrations/ -database postgres://citizenfour:12321@localhost:5432/customer up

migrate_down:
	migrate -path migrations/ -database postgres://citizenfour:12321@localhost:5432/customer down

migrate_force:
	migrate -path migrations/ -database postgres://citizenfour:12321@localhost:5432/customer force 2
