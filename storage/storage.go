package storage

import (
	"github.com/jmoiron/sqlx"
	"temp/storage/postgres"
	"temp/storage/repo"

)

type IStorage interface{
	Customer() repo.CustomerStorageI
}

type storagePg struct{
	Db		*sqlx.DB
	customerRepo repo.CustomerStorageI
}

func NewStoragePg(db *sqlx.DB) *storagePg{
	return &storagePg{
		Db: db,
		customerRepo: postgres.NewCustomerRepo(db),
	}
}

func (s storagePg) Customer() repo.CustomerStorageI{
	return s.customerRepo
}
