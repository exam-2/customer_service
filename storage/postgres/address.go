package postgres

import pc "temp/genproto/customer"

func(c *customRepo) CreateAddress(req *pc.Address) error {
	return c.DB.QueryRow(`INSERT INTO addresses(customer_id, district, street, home_number) 
	VALUES($1, $2, $3, $4)`,
	req.CustomerId, 
	req.District, 
	req.Street, 
	req.HomeNumber).Err()	
} 

func(c *customRepo) DeleteAddresses(req *pc.CustomerID) error {
	return c.DB.QueryRow(`UPDATE addresses SET updated_at=NOW() WHERE customer_id=$1 and deleted_at IS NULL`,req.Id).Err()
}

func(c *customRepo) GetAddresses(req *pc.CustomerID) ([]*pc.Address, error) {
	addresses := []*pc.Address{}
	rows, err := c.DB.Query(`SELECT customer_id, district, street, home_number FROM addresses WHERE customer_id=$1`, req.Id)
	if err != nil{	
		return nil, err
	}
	defer rows.Close()
	
	for rows.Next(){
		address := &pc.Address{}

		err := rows.Scan(
			&address.CustomerId, 
			&address.District, 
			&address.Street, 
			&address.HomeNumber)
		if err != nil{
			return nil, err 	
		}
		addresses = append(addresses, address)
	}

	return addresses, nil
}

func(c *customRepo) UpdateAddress(req *pc.Address) (*pc.Address, error) {
	responce := &pc.Address{}
	err := c.DB.QueryRow(`UPDATE addresses SET updated_at=NOW(), district=$1, street=$2, home_number=$3 WHERE 
	customer_id=$4 and deleted_at IS NULL returning customer_id, district, street, home_number`,
	req.District, 
	req.Street, 
	req.HomeNumber, 
	req.CustomerId).Scan(
		&responce.CustomerId, 
		&responce.District, 
		&responce.Street, 
		&responce.HomeNumber)
	if err != nil{
		return nil, err
	}

	return responce, err
}