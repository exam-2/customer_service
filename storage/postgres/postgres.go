package postgres

import "github.com/jmoiron/sqlx"

type customRepo struct{
	DB *sqlx.DB
}

func NewCustomerRepo(db *sqlx.DB) *customRepo{
	return &customRepo{DB: db}
}

