package postgres

import (
	"fmt"
	pc "temp/genproto/customer"
	"time"
)

func (c *customRepo) CreateCustomer(req *pc.CustomerReq) (*pc.CustomerResp, error) {
	customers := &pc.CustomerResp{}
	var createTime, updateTime time.Time
	err := c.DB.QueryRow(`
	INSERT INTO customers
		(first_name, last_name, bio, email, phone, password, code, refresh_token) 
	values
		($1, $2, $3, $4, $5, $6, $7, $8) 
	returning 
		id, 
		first_name, 
		last_name, 
		bio, email,
		phone, 
		password, 
		code
		refresh_token,
		access_token,
		created_at,
		updated_at`,  req.FirstName, 
				req.LastName, 
				req.Wikipedia, 
				req.Email, 
				req.PhoneNumber, 
				req.Password, 
				req.Code,
				req.RefreshToken).Scan(
		&customers.Id, 
		&customers.FirstName, 
		&customers.LastName, 
		&customers.Wikipedia, 
		&customers.Email, 
		&customers.PhoneNumber, 
		&customers.Password, 
		&customers.Code,
		&customers.RefreshToken,
		&customers.AccessToken,
		&createTime,
		&updateTime)	
	if err != nil {
		return &pc.CustomerResp{}, err
	}
	for _, insert := range req.Address {
		insert.CustomerId = customers.Id
		err = c.CreateAddress(insert)
		if err != nil {
			return nil, err
		}
		customers.Address = append(customers.Address, insert)
	}
	customers.CreatedAt = createTime.Format(time.RFC1123)
	customers.UpdatedAt = updateTime.Format(time.RFC1123)

	return customers, nil
}

func (c *customRepo) UpdatedCustomer(req *pc.CustomerResp) (*pc.CustomerResp, error) {
	err := c.DB.QueryRow(`update customers SET updated_at=NOW(), first_name=$1, last_name=$2, bio=$3, email=$4, phone=$5, password=$6, code=$7 
	where id = $8 and deleted_at is null`, req.FirstName, req.LastName, req.Wikipedia, req.Email, req.PhoneNumber, req.Password, req.Code, req.Id).Err()
	if err != nil {
		return &pc.CustomerResp{}, err
	}

	// for _, update := range req.Address {
	// 	err := c.CreateAddress(update)
	// 	if err != nil{
	// 		return nil, err
	// 	}
	// }
	addresses := []*pc.Address{}
	for _, insert := range req.Address {
		address, err := c.UpdateAddress(insert)
		if err != nil {
			return nil, err
		}
		addresses = append(addresses, address)
	}
	req.Address = addresses
	return req, nil
}

func (c *customRepo) DeletedCustomer(req *pc.CustomerID) error {
	err := c.DB.QueryRow(`update customers SET deleted_at=NOW() where id=$1 and deleted_at is null`, req.Id).Err()
	if err != nil {
		return err
	}
	err = c.DeleteAddresses(req)
	return err
}

func (c *customRepo) GetCustomers(req *pc.CustomerID) (*pc.GetCustomer, error) {
	customers := &pc.GetCustomer{}
	err := c.DB.QueryRow(`SELECT id, first_name, last_name, bio, email, phone, password, code FROM customers WHERE id=$1 and 
	deleted_at IS NULL`, req.Id).Scan(
		&customers.Id,
		&customers.FirstName,
		&customers.LastName,
		&customers.Wikipedia,
		&customers.Email,
		&customers.PhoneNumber,
		&customers.Password,
		&customers.Code)
	if err != nil {
		return &pc.GetCustomer{}, err
	}

	customers.Address, err = c.GetAddresses(req)
	if err != nil {
		return &pc.GetCustomer{}, err
	}

	return customers, err
}

func (c *customRepo) ListCustomers(limit, page int64) (*pc.ListCustomersResp, error) {
	responce := &pc.ListCustomersResp{}
	offset := (page - 1) * limit
	rows, err := c.DB.Query(`SELECT id, first_name, last_name, bio, email, phone, password, code FROM customers WHERE deleted_at IS NULL LIMIT $1 OFFSET $2`, limit, offset)
	if err != nil {
		return &pc.ListCustomersResp{}, err
	}
	defer rows.Close()
	for rows.Next() {
		customer := &pc.CustomerResp{}
		err := rows.Scan(
			&customer.Id,
			&customer.FirstName,
			&customer.LastName,
			&customer.Wikipedia,
			&customer.Email,
			&customer.PhoneNumber,
			&customer.Password,
			&customer.Code)
		if err != nil {
			return &pc.ListCustomersResp{}, err
		}
		responce.Customers = append(responce.Customers, customer)

		customer.Address, err = c.GetAddresses(&pc.CustomerID{Id: customer.Id})
		if err != nil {
			return &pc.ListCustomersResp{}, err
		}
	}

	return responce, nil
}

func (c *customRepo) GetByEmail(email string) (*pc.LoginResp, error) {
	res := &pc.LoginResp{}
	err := c.DB.QueryRow(`SELECT id, first_name, last_name, bio, email, password, phone  FROM 
	customers WHERE email=$1 and deleted_at is null`, email).Scan(&res.Id, &res.FirstName, &res.LastName, &res.Wikipedia, &res.Email, &res.Password, &res.PhoneNumber)
	if err != nil {
		return &pc.LoginResp{}, err
	}
	res.Address, err = c.GetAddresses(&pc.CustomerID{Id: res.Id})
	if err != nil {
		return &pc.LoginResp{}, err
	}
	return res, err
}

func (c *customRepo) GetCustomerBySearch(req *pc.GetListCustomerSearch) ([]*pc.CustomerResp, error) {
	offset := (req.Page - 1) * req.Limit
	search := fmt.Sprintf("%s LIKE $1", req.Search.Field)
	order := fmt.Sprintf("ORDER BY %s %s", req.Orders.Field, req.Orders.Value)

	query := `SELECT id, first_name, last_name, bio, email, phone, password, code from customers where deleted_at is null` + " and " + search + " " + order + " "

	rows, err := c.DB.Query(query+"LIMIT $2 OFFSET $3", "%"+req.Search.Value+"%", req.Limit, offset)
	if err != nil {
		return []*pc.CustomerResp{}, err
	}
	defer rows.Close()
	responce := []*pc.CustomerResp{}
	for rows.Next() {
		temp := &pc.CustomerResp{}
		err = rows.Scan(&temp.Id, &temp.FirstName, &temp.LastName, &temp.Wikipedia, &temp.Email, &temp.PhoneNumber, &temp.Password, &temp.Code)
		if err != nil {
			return []*pc.CustomerResp{}, err
		}
		responce = append(responce, temp)
	}
	return responce, nil
}

func (c *customRepo) GetAdmin(req *pc.GetAminReq) (*pc.GetAdminResp, error) {
	admin := &pc.GetAdminResp{}
	fmt.Println(req.Name, "<<<<--custom service")
	err := c.DB.QueryRow(`select name, password, email from admin where name=$1`, req.Name).Scan(&admin.Name, &admin.Password, &admin.Email)
	if err != nil {
		return &pc.GetAdminResp{}, err
	}
	fmt.Println(req.Name, "<<<---- query customer")
	return admin, nil
}

func (c *customRepo) GetModerator(req *pc.GetModeratorReq) (*pc.GetModeratorResp, error) {
	moderator := &pc.GetModeratorResp{}
	err := c.DB.QueryRow(`select name, password, id from moderator where name=$1`, req.Name).Scan(&moderator.Name, &moderator.Password, &moderator.Id)
	if err != nil {
		return &pc.GetModeratorResp{}, err
	}
	return moderator, nil
}
