package postgres

import (
	"fmt"
	pc "temp/genproto/customer"
	"database/sql"

)

func (c *customRepo) CheckField(req *pc.CheckFieldReq) (*pc.CheckFieldResp, error) {
	responce := &pc.CheckFieldResp{Exists: false}
	query := fmt.Sprintf(`SELECT 1 FROM customers WHERE %s=$1`,req.Field)
	temp := 0
	err := c.DB.QueryRow(query, req.Values).Scan(&temp)
	if err == sql.ErrNoRows{
		return responce, nil
	}else if err != nil{
		return responce, nil
	}
	if temp == 1 {
		responce.Exists = true
		return responce, nil
	}
	return responce, nil
}
