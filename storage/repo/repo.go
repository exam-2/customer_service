package repo

import pc "temp/genproto/customer"

type CustomerStorageI interface {
	CreateCustomer(*pc.CustomerReq) (*pc.CustomerResp, error)
	UpdatedCustomer(*pc.CustomerResp) (*pc.CustomerResp, error)
	GetCustomers(*pc.CustomerID) (*pc.GetCustomer, error)
	DeletedCustomer(*pc.CustomerID) error

	ListCustomers(limit, page int64) (*pc.ListCustomersResp, error)
	GetByEmail(email string) (*pc.LoginResp, error)
	GetCustomerBySearch(*pc.GetListCustomerSearch) ([]*pc.CustomerResp, error)

	GetAdmin(*pc.GetAminReq) (*pc.GetAdminResp, error) 
	GetModerator(*pc.GetModeratorReq) (*pc.GetModeratorResp, error) 	
	// GetAllCustomers(*pc.CustomerID) (*pc.GetCustomer, error)

	CheckField(*pc.CheckFieldReq) (*pc.CheckFieldResp, error)
	CreateAddress(*pc.Address) error
}
