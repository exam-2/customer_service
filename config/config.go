package config

import (
	"os"

	"github.com/spf13/cast"
)

// Config ...
type Config struct {
	Environment      string // develop, staging, production
	PostgresHost     string
	PostgresPort     int
	PostgresDatabase string
	PostgresUser     string
	PostgresPassword string
	LogLevel         string
	RPCPort          string

	PostServiceHost string
	PostServicePort string

	ReviewServiceHost string
	ReviewServicePort string

	KafkaHost string
	KafkaPort string
	Partitions           int
	SMSVerificationTopic string

	CustomerServiceHost string
	CustomerServicePort string
}

// Load loads environment variables and inflates Config

func Load() Config {
	c := Config{}

	c.Environment = cast.ToString(getOrReturnDefault("ENVIRONMENT", "develop"))

	c.PostgresHost = cast.ToString(getOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToInt(getOrReturnDefault("POSTGRES_PORT", 5432))
	c.PostgresDatabase = cast.ToString(getOrReturnDefault("POSTGRES_DATABASE", "customer"))
	c.PostgresUser = cast.ToString(getOrReturnDefault("POSTGRES_USER", "citizenfour"))
	c.PostgresPassword = cast.ToString(getOrReturnDefault("POSTGRES_PASSWORD", "12321"))

	c.KafkaHost = cast.ToString(getOrReturnDefault("KAFKA_HOST", "kafka"))
	c.KafkaPort = cast.ToString(getOrReturnDefault("KAFKA_PORT", "29092"))
	c.SMSVerificationTopic = cast.ToString(getOrReturnDefault("SMS_VERIFICATION_TOPIC", "sms_verification"))
	c.Partitions = cast.ToInt(getOrReturnDefault("PARTITIONS", 0))

	c.PostServiceHost = cast.ToString(getOrReturnDefault("POST_SERVICE_HOST", "localhost"))
	c.PostServicePort = cast.ToString(getOrReturnDefault("POST_SERVICE_PORT", "2222"))

	c.CustomerServiceHost = cast.ToString(getOrReturnDefault("CUSTOMER_SERVICE_HOST", "localhost"))
	c.CustomerServicePort = cast.ToString(getOrReturnDefault("CUSTOMER_SERVICE_PORT", "3333"))

	c.ReviewServiceHost = cast.ToString(getOrReturnDefault("REVIEW_SERVICE_HOST", "localhost"))
	c.ReviewServicePort = cast.ToString(getOrReturnDefault("REVIEW_SERVICE_PORT", "1111"))

	c.LogLevel = cast.ToString(getOrReturnDefault("LOGLEVEL", "debug"))
	c.RPCPort = cast.ToString(getOrReturnDefault("RPC_PORT", ":3333"))

	return c
}

func getOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
