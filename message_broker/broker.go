package messagebroker

import (
	"context"
	"encoding/json"
	"time"

	"github.com/segmentio/kafka-go"
	"temp/config"
	"temp/message_broker/models"
)

type Producer struct {
	Conn      *kafka.Conn
	CloseConn func()
}

func NewProducer(cfg config.Config) (*Producer, error) {
	for {
		conn, err := kafka.DialLeader(context.Background(), "tcp", cfg.KafkaHost+":"+cfg.KafkaPort, cfg.SMSVerificationTopic, cfg.Partitions)
		if err == nil {
			return &Producer{
				Conn: conn,
				CloseConn: func() {
					conn.Close()
				},
			}, nil
		}
		time.Sleep(time.Second * 10)
	}
}

func (p *Producer) SendSMSVerification(message *models.SendSMSVerificationCodeReq) error {
	value, err := json.Marshal(message)
	if err != nil {
		return err
	}
	_, err = p.Conn.WriteMessages(kafka.Message{
		Value: value,
	})
	return err
}
